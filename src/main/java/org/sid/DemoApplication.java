package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sid.Dao.ContactRepository;
import org.sid.Entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication  implements CommandLineRunner {

		@Autowired
		private ContactRepository contactRepository;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
	
		DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		
//		contactRepository.save(new Contact("nzapa", "narcisse",df.parse("12/01/1992"),"narciss@gmail.com", new Long(670123120),"imphotos"));
//		contactRepository.save(new Contact("yemelong", "Romuald",df.parse("12/01/1991"),"romual@gmail.com", new Long(67012820),"im.photos"));
//		contactRepository.save(new Contact("Moffo", "calvin",df.parse("12/01/1994"),"calvin@gmail.com", new Long(677923120),"imphotos"));
		contactRepository.findAll().forEach(c ->{
			
			System.out.println(c.getNom());
		});
	}
}
